import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageServicosComponent } from './page-servicos.component';

describe('PageServicosComponent', () => {
  let component: PageServicosComponent;
  let fixture: ComponentFixture<PageServicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageServicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageServicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
