import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import { Component, OnInit } from '@angular/core';
import { PageEquipeModalComponent } from './page-equipe-modal/page-equipe-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-page-equipe',
  templateUrl: './page-equipe.component.html',
  styleUrls: ['./page-equipe.component.css'],
})
export class PageEquipeComponent implements OnInit {

  constructor(public dialog: MatDialog){}

  equipeModal(value){
    var profissional = new Object(); 
    if(value == 1){ profissional["name"]= "Amanda Rodrigues Goncalves"; profissional["foto"]= "././assets/avatar1.png";}
    if(value == 2){ profissional["name"]= "Alex Gomes Cardoso"; profissional["foto"]= "././assets/avatar2.png";}
    if(value == 3){ profissional["name"]= "Nicole Castro Martins"; profissional["foto"]= "././assets/avatar3.png";}
    if(value == 4){ profissional["name"]= "Marina Dias Barros"; profissional["foto"]= "././assets/avatar4.png";}
    if(value == 5){ profissional["name"]= "Isabelle Alves Souza"; profissional["foto"]= "././assets/avatar5.png";}
    if(value == 6){ profissional["name"]= "Fernanda Barros Sousa"; profissional["foto"]= "././assets/avatar6.png";}

    let dialogRef = this.dialog.open(PageEquipeModalComponent, {
      panelClass: 'custom-dialog-container',
      height: '800px',
      width: '1000px',
      data: profissional,
      
    });
  }

  ngOnInit() {
  }

}
