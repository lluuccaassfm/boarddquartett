import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material";


@Component({
  selector: 'app-page-equipe-modal',
  templateUrl: './page-equipe-modal.component.html',
  styleUrls: ['./page-equipe-modal.component.css']
})
export class PageEquipeModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data: String) { }

  ngOnInit() {
  }

}
