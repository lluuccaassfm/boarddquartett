import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEquipeModalComponent } from './page-equipe-modal.component';

describe('PageEquipeModalComponent', () => {
  let component: PageEquipeModalComponent;
  let fixture: ComponentFixture<PageEquipeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEquipeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEquipeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
