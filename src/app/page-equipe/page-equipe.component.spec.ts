import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEquipeComponent } from './page-equipe.component';

describe('PageEquipeComponent', () => {
  let component: PageEquipeComponent;
  let fixture: ComponentFixture<PageEquipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEquipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
