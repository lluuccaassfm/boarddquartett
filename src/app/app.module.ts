import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PRIMENG_IMPORTS } from './primeng-imports';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import { HeaderComponent } from './header/header.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { FooterComponent } from './footer/footer.component';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { PageEquipeComponent } from './page-equipe/page-equipe.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { PageEquipeModalComponent } from './page-equipe/page-equipe-modal/page-equipe-modal.component';
import { PageServicosComponent } from './page-servicos/page-servicos.component';
import { PageDepoimentoComponent } from './page-depoimento/page-depoimento.component';


const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'servicos', component: PageServicosComponent },
  { path: 'equipe', component: PageEquipeComponent },
  { path: 'depoimento', component: PageDepoimentoComponent },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    HomeComponent,
    PageEquipeComponent,
    PageEquipeModalComponent,
    PageServicosComponent,
    PageDepoimentoComponent
  ],
  entryComponents: [PageEquipeModalComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatDialogModule,
    RouterModule.forRoot(appRoutes),
    PRIMENG_IMPORTS
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
